# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

## HTML & CSS Task

## Objective
* Checkout the dev branch.
* Add meta tag which resizes the viewport to the width of the screen in CSS pixels at a scale of 100% and set the initial scale to 1.
* When implemented merge the dev branch to master.
## Requirements
* Start the project with **npm run start**.
* The website must contain the viewport meta tag.
* The website must have the initial scale set to 1.
